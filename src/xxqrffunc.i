/******************************************************************************/
/* xxqrffunc.i - Strategic Include for submitting QRF reports to batch      */
/* Author      : Dumitru Cotfas                                               */
/* Date Created: 14 - April - 2017                                                 */
/* Modification History                                                       */
/******************************************************************************/
/* -----------------------------------------------------------------------
   Interface functions :
        xqrf-init-batch             -- Initialize for a new batch submission
                input Report Code       (ie, "SIG_InvoiceReprintForm")
                input Batch ID          (blank for the immediate batch)
        xqrf-reportDefinition       -- Specify an alternate report definition
                input reportDefinition  (name of report definition if you want
                                         to override the default definition)
        xqrf-description            -- Specify a description for the batch job
                input description       (description for batch job)
        xqrf-printer                -- Specify a printer name to print report
                input printer           (printer UNC name)
        xqrf-output-path            -- Override default directory for saved file
                input output-path       (directory name, it will still be
                                         under Tomcat storage directory)
        xqrf-output-file            -- Override default file name for saved file
                input output-file       (file name)
        xqrf-parameter              -- Specify report selection parameters
                input fieldName         (field name to specify values for,
                                         examples: 'tt_ih_hist.ih_inv_nbr',
                                                   'ttreportoptions.show_comments')
                input Operator          (operator for selection criteria)
                                        "="
                                        "EQ"
                                        ">"
                                        "GT"
                                        "<"
                                        "LT"
                                        "<>"
                                        "NE"
                                        "NULL"
                                        "NOTNULL"
                                        "RANGE"
                                        "CONTAINS"
                                        "STARTS"
                                        "STARTSAT"
                input Value1            (value #1 in selection criteria)
                                        Date values use YYYY-MM-DD string value
                                        Logical values use "TRUE" or "FALSE",
                                                "YES" or "NO" do not work
                input Value2            (value #2 in selection criteria, for 'RANGE' operator)
                                        Date values use YYYY-MM-DD string value
                                        Logical values use "TRUE" or "FALSE",
                                                "YES" or "NO" do not work
        xqrf-email-info             -- Specify information for generating e-mail
                input emailAddresses    (comma separated list of e-mail addresses)
                input emailSubject      (subject text for e-mail)
                input emailBody         (body text for e-mail)
        xqrf-submit                 -- Submit report to batch
                output reportID         (report ID in batch)
                output errorCode        (error code if submission fails)
                input Mode              ("API" for API call,
                                         "TABLE" for direct table update)
   ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
    Required functions for submitting to a report to batch:
    xqrf-init-batch
    xqrf-submit
   ----------------------------------------------------------------------- */

function xqrf-init-batch returns integer
    (i-report-code as character,
     i-batch-id    as character)
    forward.

function xqrf-submit returns integer
    (output oiScheduledReportID as integer,
     output oiErrorCode         as integer,
     input  xqrf-Mode           as character)
    forward.

/* -----------------------------------------------------------------------
    Optional functions for submitting to a report to batch:
    xqrf-parameter
    xqrf-reportDefinition
    xqrf-description
    xqrf-printer
    xqrf-email-info
    xqrf-output-path
    xqrf-output-file
   ----------------------------------------------------------------------- */

function xqrf-parameter returns integer
    (i-field  as character, i-operator as character,
     i-value1 as character, i-value2   as character)
    forward.

function xqrf-reportDefinition returns integer
    (i-value as character)
    forward.

function xqrf-description returns integer
    (i-value as character)
    forward.

function xqrf-printer returns integer
    (i-value as character)
    forward.

function xqrf-email-info returns integer
    (i-email-addresses as character,
     i-email-subject   as character,
     i-email-body      as character)
    forward.

function xqrf-output-path returns integer
    (i-value as character)
    forward.

function xqrf-output-file returns integer
    (i-value as character)
    forward.

/* -----------------------------------------------------------------------
    Internal functions:
    f-email-template
    f-qrf-cleanup
   ----------------------------------------------------------------------- */

function f-email-template returns character
    (input subj-txt as character,
     input body-txt as character)
    forward.

function f-xqrf-cleanup returns integer
    forward.


/* -----------------------------------------------------------------------
    Code for submitting reports to batch follows.
   ----------------------------------------------------------------------- */

/*Report API temp tables and constants*/
{us/rp/rprfapi.i}
{us/rp/rprfcons.i}
{us/rp/rprfcode.i}

define variable xqrf-Session as handle no-undo.
define variable xqrf-Service as handle no-undo.

/*Service interface include files*/
{us/ex/excpmgpr.i super}
{us/se/sectxtpr.i xqrf-Service}
{us/ds/dsexcptn.i}

/****************MANDATORY PROCEDURES AND FUNCTIONS************************/

/* -----------------------------------------------------------------------
   xqrf-init-batch returns integer
        Initialize batch dataset

            input i-report-code     -- Report code to submit to batch
            input i-batch-id        -- Batch ID to submit report against

        Paul Bonn, Strategic Information Group, 2014-Nov-21
   ----------------------------------------------------------------------- */

function xqrf-init-batch returns integer
    (i-report-code as character,
     i-batch-id    as character)
    :
    define buffer ttSchedRptBuf for ttScheduleReportAPI.

    f-xqrf-cleanup().

    create ttSchedRptBuf.
    assign
        ttSchedRptBuf.rroCode        = i-report-code
        ttSchedRptBuf.batchID        = i-batch-id

        ttSchedRptBuf.domain         = global_domain
        ttSchedRptBuf.entity         = current_entity
        ttSchedRptBuf.appLanguage    = global_user_lang
        ttSchedRptBuf.isoLanguage    = ""
        ttSchedRptBuf.runOnlyOnce    = true
        ttSchedRptBuf.saveReport     = true
        ttSchedRptBuf.emailAddresses = ""
        ttSchedRptBuf.linkToEmail    = false.
        ttSchedRptBuf.attachToEmail  = false.

    return 0.
end function.       /* xqrf-init-batch */


/* ----------------------------------------------------------------------- */


/* -----------------------------------------------------------------------
   xqrf-submit returns integer
        Submit job to QAD reporting framework batch, using current mode setting
            output oiScheduledReportID      -- report sequence ID in the batch
            output oiErrorCode              -- error code if submit fails
            input  xqrf-Mode                -- "TABLE" for direct table update
                                               "API" to call the QAD API
        Paul Bonn, Strategic Information Group, 2014-Nov-21
   ----------------------------------------------------------------------- */
function xqrf-submit returns integer
    (output oiScheduledReportID as integer,
     output oiErrorCode         as integer,
     input  xqrf-Mode           as character)
    :

    if xqrf-Mode = "TABLE" or xqrf-Mode = "DIRECT" then
        run p-submit-table(output oiScheduledReportID,
                           output oiErrorCode).
    else
        run p-submit-api(output oiScheduledReportID,
                         output oiErrorCode).

    return oiScheduledReportID.
end function.       /* xqrf-submit */
/* ----------------------------------------------------------------------- */


/****************OPTIONAL PROCEDURES AND FUNCTIONS************************/


/* -----------------------------------------------------------------------
   xqrf-parameter returns integer
        <function description>

        Paul Bonn, Strategic Information Group, 2014-Nov-21
   ----------------------------------------------------------------------- */
function xqrf-parameter returns integer
    (i-field  as character, i-operator as character,
     i-value1 as character, i-value2   as character)
    :

    case i-operator:
        when ""         then i-operator = {&PARAMETER_OPERATOR_EQUALS}.
        when "="        then i-operator = {&PARAMETER_OPERATOR_EQUALS}.
        when "EQ"       then i-operator = {&PARAMETER_OPERATOR_EQUALS}.
        when ">"        then i-operator = {&PARAMETER_OPERATOR_GREATERTHAN}.
        when "GT"       then i-operator = {&PARAMETER_OPERATOR_GREATERTHAN}.
        when "<"        then i-operator = {&PARAMETER_OPERATOR_LESSTHAN}.
        when "LT"       then i-operator = {&PARAMETER_OPERATOR_LESSTHAN}.
        when "<>"       then i-operator = {&PARAMETER_OPERATOR_NOTEQUALS}.
        when "NE"       then i-operator = {&PARAMETER_OPERATOR_NOTEQUALS}.
        when "NULL"     then i-operator = {&PARAMETER_OPERATOR_ISNULL}.
        when "NOTNULL"  then i-operator = {&PARAMETER_OPERATOR_ISNOTNULL}.
        when "RANGE"    then i-operator = {&PARAMETER_OPERATOR_RANGE}.
        when "CONTAINS" then i-operator = {&PARAMETER_OPERATOR_CONTAINS}.
        when "STARTS"   then i-operator = {&PARAMETER_OPERATOR_STARTSAT}.
        when "STARTSAT" then i-operator = {&PARAMETER_OPERATOR_STARTSAT}.
    end case.    /* i-operator */

    define buffer ttParmBuf for ttParameterAPI.

    create ttParmBuf.
    assign
        ttParmBuf.name            = i-field
        ttParmBuf.operator        = i-operator
        ttParmBuf.paramValue      = i-value1
        ttParmBuf.valueType       = {&PARAMETER_VALUETYPE_CONSTANT}
        ttParmBuf.secondValue     = i-value2
        ttParmBuf.secondValueType = {&PARAMETER_VALUETYPE_CONSTANT}
        .

    return 0.
end function.       /* xqrf-parameter */
/* ----------------------------------------------------------------------- */


/* -----------------------------------------------------------------------
   xqrf-reportDefinition returns integer
        Set the report definition to submit
            input i-value       -- Report definition name

        Paul Bonn, Strategic Information Group, 2014-Nov-21
   ----------------------------------------------------------------------- */
function xqrf-reportDefinition returns integer
    (i-value as character):
    define buffer ttSchedRptBuf for ttScheduleReportAPI.

    for first ttSchedRptBuf:
        ttSchedRptBuf.reportDefinition = i-value.
    end.

    return 0.
end function.       /* xqrf-reportDefinition */
/* ----------------------------------------------------------------------- */


/* -----------------------------------------------------------------------
   xqrf-description returns integer
        Set the batch job's description
            input i-value       -- Batch job description

        Paul Bonn, Strategic Information Group, 2014-Nov-21
   ----------------------------------------------------------------------- */
function xqrf-description returns integer
    (i-value as character):
    define buffer ttSchedRptBuf for ttScheduleReportAPI.

    for first ttSchedRptBuf:
        ttSchedRptBuf.description = i-value.
    end.

    return 0.
end function.       /* xqrf-description */
/* ----------------------------------------------------------------------- */


/* -----------------------------------------------------------------------
   xqrf-printer returns integer
        Set the UNC path to a printer if we should print this job
            input i-value       -- Print UNC path

        Paul Bonn, Strategic Information Group, 2014-Nov-21
   ----------------------------------------------------------------------- */
function xqrf-printer returns integer
    (i-value as character):
    define buffer ttSchedRptBuf for ttScheduleReportAPI.

    for first ttSchedRptBuf:
        ttSchedRptBuf.printer = i-value.
    end.

    return 0.
end function.       /* xqrf-printer */
/* ----------------------------------------------------------------------- */


/* -----------------------------------------------------------------------
   xqrf-email-info returns integer
        Update batch with e-mail information
            input i-email-addresses         -- Comma separated list of e-mail addresses
            input i-email-subject           -- E-mail subject line
            input i-email-body              -- E-mail body text

        Paul Bonn, Strategic Information Group, 2014-Nov-21
   ----------------------------------------------------------------------- */
function xqrf-email-info returns integer
    (i-email-addresses as character,
     i-email-subject   as character,
     i-email-body      as character)
    :

    define buffer ttSchedRptBuf for ttScheduleReportAPI.

    for first ttSchedRptBuf:
        if i-email-addresses <> "" then do:
            ttSchedRptBuf.emailAddresses = i-email-addresses.
            ttSchedRptBuf.attachToEmail  = true.

            if i-email-subject <> "" or i-email-body <> "" then
                ttSchedRptBuf.emailTemplate = f-email-template(i-email-subject, i-email-body).
        end.
    end.

    return 0.
end function.       /* xqrf-email-info */
/* ----------------------------------------------------------------------- */


/* -----------------------------------------------------------------------
   xqrf-output-path returns integer
        Directory to store saved report output into.  This directory will
        be located under the Tomcat "storage" directory still
            input i-value       -- Directory name

        Paul Bonn, Strategic Information Group, 2014-Nov-21
   ----------------------------------------------------------------------- */
function xqrf-output-path returns integer
    (i-value as character):

    if i-value <> "" then do:
        if not i-value matches "*/" then
            i-value = i-value + "/".

        i-value = replace(i-value, '~*', '').
        i-value = replace(i-value, '~&', '').
        i-value = replace(i-value, '~>', '').
        i-value = replace(i-value, '~<', '').
        i-value = replace(i-value, '~#', '').
        i-value = replace(i-value, '~;', '').
        i-value = replace(i-value, '~:', '').
        i-value = replace(i-value, "~'", '').
        i-value = replace(i-value, "~`", '').
        i-value = replace(i-value, '~"', '').
        i-value = replace(i-value, '~,', '').
        i-value = replace(i-value, '~!', '').
        i-value = replace(i-value, '~?', '').
        i-value = replace(i-value, '~$', '').
        i-value = replace(i-value, '~(', '').
        i-value = replace(i-value, '~)', '').
        i-value = replace(i-value, '~[', '').
        i-value = replace(i-value, '~]', '').
        i-value = replace(i-value, '~{', '').
        i-value = replace(i-value, '~}', '').
        i-value = replace(i-value, '~|', '').
        i-value = replace(i-value, '~~', '').
        i-value = replace(i-value, '~\', '').

        xqrf-parameter('sys_output_file_path', '=', i-value, '').
    end.

    return 0.
end function.       /* xqrf-output-path */
/* ----------------------------------------------------------------------- */


/* -----------------------------------------------------------------------
   xqrf-output-file returns integer
        Override the default file-name for the generated report output
            input i-value       -- File name

        Paul Bonn, Strategic Information Group, 2014-Nov-21
   ----------------------------------------------------------------------- */
function xqrf-output-file returns integer
    (i-value as character):

    if i-value <> "" then do:
        i-value = replace(i-value, '~/', '').
        i-value = replace(i-value, '~*', '').
        i-value = replace(i-value, '~&', '').
        i-value = replace(i-value, '~>', '').
        i-value = replace(i-value, '~<', '').
        i-value = replace(i-value, '~#', '').
        i-value = replace(i-value, '~;', '').
        i-value = replace(i-value, '~:', '').
        i-value = replace(i-value, "~'", '').
        i-value = replace(i-value, "~`", '').
        i-value = replace(i-value, '~"', '').
        i-value = replace(i-value, '~,', '').
        i-value = replace(i-value, '~!', '').
        i-value = replace(i-value, '~?', '').
        i-value = replace(i-value, '~$', '').
        i-value = replace(i-value, '~(', '').
        i-value = replace(i-value, '~)', '').
        i-value = replace(i-value, '~[', '').
        i-value = replace(i-value, '~]', '').
        i-value = replace(i-value, '~{', '').
        i-value = replace(i-value, '~}', '').
        i-value = replace(i-value, '~|', '').
        i-value = replace(i-value, '~~', '').
        i-value = replace(i-value, '~\', '').

        xqrf-parameter('sys_output_file_name', '=', i-value, '').
    end.

    return 0.
end function.       /* xqrf-output-file */
/* ----------------------------------------------------------------------- */


/****************INTERNAL PROCEDURES AND FUNCTIONS************************/

/* -----------------------------------------------------------------------
   f-xqrf-cleanup returns integer
        Purge temp-table contents

        Paul Bonn, Strategic Information Group, 2014-Nov-21
   ----------------------------------------------------------------------- */
function f-xqrf-cleanup returns integer :
    for each ttScheduleReportAPI exclusive-lock:
        delete ttScheduleReportAPI.
    end.

    for each ttParameterAPI exclusive-lock:
        delete ttParameterAPI.
    end.

    for each temp_err_msg exclusive-lock:
        delete temp_err_msg.
    end.

    for each ttResponseAPI exclusive-lock:
        delete ttResponseAPI.
    end.

    run clearExceptions.

    return 0.
end function.       /* f-xqrf-cleanup */
/* ----------------------------------------------------------------------- */


/* -----------------------------------------------------------------------
   f-email-template returns character
        Generates an e-mail template to use to override default e-mail
        template used when e-mailing batched report output
            input subj-txt          -- Subject line text
            input body-txt          -- Body text.

        Paul Bonn, Strategic Information Group, 2014-Nov-21
   ----------------------------------------------------------------------- */
function f-email-template returns character
    (input subj-txt as character,
     input body-txt as character) :

    define variable ret-val as character.

    ret-val = "[SUBJECT]" + chr(13)
            + subj-txt    + chr(13)
            + "[BODY]"    + chr(13)
            + body-txt    + chr(13) + chr(13) + chr(13).

    return ret-val.
end function.       /* f-email-template */
/* ----------------------------------------------------------------------- */


/* -----------------------------------------------------------------------
   processOutput
        Process the batch job submission output

        Paul Bonn, Strategic Information Group, 2014-Nov-21
   ----------------------------------------------------------------------- */
/*
    This procedure can be modified to log or display errors as appropriate.
    and to return or log the scheduled report ID. The error codes
    returned via ttResponseAPI.errorCode can be found in rpAPIErrorCodes.i
*/
procedure processOutput:
   define output parameter oiScheduledReportID as integer.
   define output parameter oiErrorCode as integer.
   define variable l_msg_desc as character no-undo.

   /*These are the errors from the service interface layer.*/
   run getUnhandledExceptions(output dataset dsExceptions).
   for each temp_err_msg no-lock:
       assign l_msg_desc = tt_msg_desc + " (" + tt_msg_nbr + ")".
       {us/bbi/pxmsg.i &MSGTEXT=l_msg_desc &ERRORLEVEL=3}
   end.

   /*These are the errors/info from the scheduling itself.
     See rpAPIErrorCodes.i for error codes.*/
   for first ttResponseAPI no-lock:
      if ttResponseAPI.success = false then do:
         oiScheduledReportID = -1.
         oiErrorCode = ttResponseAPI.errorCode.
      end.
      else do:
         oiScheduledReportID = ttResponseAPI.scheduledReportId.
         oiErrorCode = -1.
      end.
   end.
end procedure.      /* processOutput */
/* ----------------------------------------------------------------------- */


/* -----------------------------------------------------------------------
   login
        Sets the login parameters to the service interface layer

        Paul Bonn, Strategic Information Group, 2014-Nov-21
   ----------------------------------------------------------------------- */
procedure login:
   run setQADContextProperty in xqrf-Service ("sessionID", mfguser).
   run setQADContextProperty in xqrf-Service ("domain"  ,  global_domain).
end procedure.      /* login */
/* ----------------------------------------------------------------------- */


/* -----------------------------------------------------------------------
   ScheduleReport
        Call the scheduling logic.  Only one ttScheduleReportAPI per call.

        Paul Bonn, Strategic Information Group, 2014-Nov-21
   ----------------------------------------------------------------------- */
procedure ScheduleReport:
    define variable sid as character no-undo.

    run setQADContextProperty in xqrf-Service ("domain", global_domain).

    do on error undo, leave:
        run ScheduleReport in xqrf-Service
            (        dataset dsScheduleReportAPIRequest,
              output dataset dsScheduleReportAPIResponse
            ).
    end.
end procedure.      /* ScheduleReport */
/* ----------------------------------------------------------------------- */


/* -----------------------------------------------------------------------
   p-submit-api
        Submit job to QAD reporting framework batch, using API calls
            output oiScheduledReportID      -- report sequence ID in the batch
            output oiErrorCode              -- error code if submit fails

        Paul Bonn, Strategic Information Group, 2016-Feb-05
   ----------------------------------------------------------------------- */
procedure p-submit-api:
    define output parameter oiScheduledReportID as integer.
    define output parameter oiErrorCode         as integer.

    /*Initialize the service interface programs*/
    run com/qad/qra/si/SessionManager.p
        persistent set xqrf-Session.
    run com/qad/shell/report/ScheduleReportAPIProxyInternal.p
        persistent set xqrf-Service.
    run initialize in xqrf-Service (?, ?, search("server.xml")).

    run setQADContextProperty in xqrf-Service ("serviceInstance", "MfgPro").

    run login.

    run ScheduleReport.
    run processOutput(output oiScheduledReportID, output oiErrorCode).
    f-xqrf-cleanup().

    if valid-handle(xqrf-Service) then do:
        /* apply 'close' to xqrf-Service.*/
        delete procedure xqrf-Service no-error.
    end.
    if valid-handle(xqrf-Session) then do:
        apply 'close' to xqrf-Session.
        delete procedure xqrf-Session no-error.
    end.
end procedure.       /* p-submit-api */
/* ----------------------------------------------------------------------- */


/* -----------------------------------------------------------------------
   p-submit-table
        Submit job to QAD reporting framework batch, using direct table writes
            output oiScheduledReportID      -- report sequence ID in the batch
            output oiErrorCode              -- error code if submit fails

        Paul Bonn, Strategic Information Group, 2016-Feb-05
   ----------------------------------------------------------------------- */
procedure p-submit-table:
    define output parameter oiScheduledReportID as integer.
    define output parameter oiErrorCode         as integer.

    define buffer ttParmBuf     for ttParameterAPI.
    define buffer ttSchedRptBuf for ttScheduleReportAPI.

    define variable cur_par_id as integer.

    oiErrorCode = -1.

    find first ttSchedRptBuf no-lock
            no-error.
    if not available ttSchedRptBuf then do:
        oiErrorCode = -2.
        return.
    end.    /* if not available ttSchedRptBuf then */

    find rptres_mstr no-lock
            where rptres_domain = ""
              and rptres_code   = ttSchedRptBuf.rrocode
            no-error.
    if not available rptres_mstr then do:
        oiErrorCode = -3.
        return.
    end.    /* if not available rptres_mstr then */

    find rptresd_det no-lock
            where rptresd_id = rptres_def_rptresd_id
            no-error.
    if not available rptresd_det then do:
        oiErrorCode = -4.
        return.
    end.    /* if not available rptresd_det then */

    if ttSchedRptBuf.reportDefinition <> "" then do:
        find rptresd_det no-lock
                where rptresd_rptres_id = rptres_id
                  and rptresd_name      = ttSchedRptBuf.reportDefinition
                no-error.
        if not available rptresd_det then do:
            oiErrorCode = -5.
            return.
        end.    /* if not available rptresd_det then */
    end.    /* if ttSchedRptBuf.reportDefinition <> "" then */

    do transaction:
        create rptsr_mstr.
        assign
            rptsr_id             = next-value(rptsr_sq01)
            rptsr_rptres_id      = rptres_id
            rptsr_usr_userid     = global_userid
            rptsr_create_date    = today
            rptsr_create_time    = mtime
            rptsr_rptres_version = rptres_version
            rptsr_mod_date       = today
            rptsr_domain         = global_domain
            rptsr_permanent      = (not ttSchedRptBuf.runOnlyOnce)
            rptsr_description    = ttSchedRptBuf.description
            /* Place on hold until all parameters are configured */
            rptsr_bc_batch       = "ZZHOLDZZ"
            rptsr_status         = "ZZHOLDZZ"
            rptsr_active         = NO
            .

        xqrf-parameter('sys_mfg_language',  '=', ttSchedRptBuf.appLanguage, '').
        xqrf-parameter('sys_base_currency', '=', base_curr, '').

        if ttSchedRptBuf.emailAddresses <> "" then do:
            xqrf-parameter('sys_email', '=', ttSchedRptBuf.emailAddresses, '').
            if ttSchedRptBuf.attachToEmail then
                xqrf-parameter('sys_attach_to_email', '=', 'True', '').
            else
                xqrf-parameter('sys_attach_to_email', '=', 'False', '').
            if ttSchedRptBuf.emailTemplate <> "" then
                xqrf-parameter('sys_email_template1', '=', ttSchedRptBuf.emailTemplate, '').
        end.    /* if ttSchedRptBuf.emailAddresses <> "" then */

        if ttSchedRptBuf.saveReport then
            xqrf-parameter('sys_save_output', '=', 'True', '').
        else
            xqrf-parameter('sys_save_output', '=', 'False', '').

        xqrf-parameter('sys_render_as',                 '=', '1', '').
        xqrf-parameter('sys_default_report_definition', '=', rptresd_name, '').
        xqrf-parameter('sys_domain',                    '=', ttSchedRptBuf.domain, '').
        xqrf-parameter('sys_trusted_signon_session',    '=', global_userid, '').
        xqrf-parameter('sys_ips',                       '=', 'API', '').
        xqrf-parameter('sys_search_criteria_display',   '=', '1', '').
        xqrf-parameter('sys_entity',                    '=', ttSchedRptBuf.entity, '').
        xqrf-parameter('sys_ci_short_date_pattern',     '=', 'yyyy-MM-dd', '').
        xqrf-parameter('sys_ci_date_separator',         '=', '/', '').
        xqrf-parameter('sys_ci_decimal_separator',      '=', '.', '').
        xqrf-parameter('sys_ci_decimal_digits',         '=', '2', '').
        xqrf-parameter('sys_ci_group_separator',        '=', ',', '').
        xqrf-parameter('sys_ci_group_sizes',            '=', '3', '').

        if ttSchedRptBuf.printer <> "" then
            xqrf-parameter('sys_printer', '=', ttSchedRptBuf.printer, '').

        cur_par_id = 0.

        for each ttParmBuf no-lock :
            cur_par_id = cur_par_id + 1.
            create rptsrp_mstr.
            assign
                rptsrp_id            = next-value(rptsrp_sq01)
                rptsrp_rptsr_id      = rptsr_id
                rptsrp_rptpmt_id     = cur_par_id
                rptsrp_name          = ttParmBuf.name
                rptsrp_value         = ttParmBuf.paramValue
                rptsrp_valuetype     = ttParmBuf.valueType
                rptsrp_scd_value     = ttParmBuf.secondValue
                rptsrp_scd_valuetype = ttParmBuf.secondValueType
                rptsrp_operator      = ttParmBuf.operator
                oid_rptsrp_mstr      = rptsrp_id
                rptsrp_mod_date      = today
                .
        end.    /* for each ttParmBuf */

        /* Take off hold so batch processor will consider this batch */
        assign
            rptsr_bc_batch    = (if ttSchedRptBuf.batchID = ""
                                 then 'CSSBATCH'
                                 else ttSchedRptBuf.batchID)
            rptsr_status      = "NEW"
            rptsr_active      = YES.
        oiScheduledReportID = rptsr_id.
    end.    /* do transaction */
    release rptsr_mstr.   
    release rptsrp_mstr.  
    f-xqrf-cleanup().
end procedure.       /* p-submit-table */
/* ----------------------------------------------------------------------- */

